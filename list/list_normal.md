# Linked List dengan fungsi push_back

## Kode

```c++
#include <iostream>

struct node {
    int data;
    node * next;
};

struct list {
    node *head, *tail;
};

// Inisialisasi object list baru
void init(list * object){
    object->head = NULL;
    object->tail = NULL;
}

void push_back(list * object, int data){
    node * unlinked_node = new node;

    unlinked_node->data = data;
    unlinked_node->next = NULL;

    // Jika list sudah ada isinya ke sini
    if(object->tail != NULL){
        object->tail->next = unlinked_node;
        object->tail = unlinked_node;
    }
    else {
        object->head = unlinked_node;
        object->tail = unlinked_node;
    }
}

// Mencetak isi list secara berurutan
void print(list * object){
    node * iter = object->head;
    while(iter != NULL){
        std::cout << iter->data << " ";
        iter = iter->next;
    }
    std::cout << std::endl;
}

int main(){
    int n, temp_int;
    std::cin >> n;
    list linked_list;
    init(&linked_list);

    while(n--){
        std::cin >> temp_int;
        push_back(&linked_list, temp_int);
    }

    print(&linked_list);
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183387.js" id="asciicast-183387" async></script>