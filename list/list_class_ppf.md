# Linked List dengan fungsi pop_front dan abstraksi Class

## Perubahan Kode

Fungsi dari pop_front sendiri di sini adalah untuk menghapus node yang ada paling belakang list, penambahan kodenya yaitu

```c++
void pop_front(){
    // Algoritma mirip, namun code lebih singkat dan padat
    auto will_be_head = head->next;
    head.reset();
    head = will_be_head;
}
```

## Kode

Secara umum keseluruhan kode akan menjadi

```c++
#include <iostream>
// Penggunaan <memory> untuk memory management
// seperti pengalokasian pointer di C++ Modern
#include <memory>

struct node {
    int data;
    // Shared pointer merupakan salah satu fitur baru
    // yang dikenalkan pada C++11
    std::shared_ptr<node> next;
};

class list {
  private:
    std::shared_ptr<node> head, tail;
  
  public:
    list(){
        head = nullptr;
        tail = nullptr;
    }

    void push_back(int data){
        // Penggunaan auto merupakan upaya dari C++ compiler
        // secara otomatis menebak tipe data dari yang kita inginkan
        // Hal ini berbeda dengan dynamic variable yang ada di bahasa
        // pemrogaman tingkat tinggi seperti Python dan Javascript
        // Make shared artinya kita membuat pointer shared_ptr dari node
        auto unlinked_node = std::make_shared<node>();

        unlinked_node->data = data;
        unlinked_node->next = nullptr;

        if(tail != nullptr){
            tail->next = unlinked_node;
            tail = unlinked_node;
        }
        else {
            head = unlinked_node;
            tail = unlinked_node;
        }
    }

    void pop_front(){
        // Algoritma mirip, namun code lebih singkat dan padat
        auto will_be_head = head->next;
        head.reset();
        head = will_be_head;
    }

    void pop_back(){
        // Penggunaan auto sangat memudahkan untuk menghindari terjadinya
        // salah tipe
        auto will_be_tail = head;

        while(will_be_tail->next != tail)
            will_be_tail = will_be_tail->next;

        will_be_tail->next = nullptr;

        // Jika pada C99 kita melakukan delete
        // Maka mulai C++11 ada opsi untuk meresetnya saja
        // ke bentuk yang awal
        tail.reset();
        tail = will_be_tail;
    }

    // Cetak satu per satu
    void print(){
        auto iter = head;
        while(iter != nullptr){
            std::cout << iter->data << " ";
            iter = iter->next;
        }
        std::cout << std::endl;
    }
};

int main(){
    int n, temp_int;
    std::cin >> n;
    list linked_list;

    while(n--){
        std::cin >> temp_int;
        linked_list.push_back(temp_int);
    }

    linked_list.pop_back();

    linked_list.print();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183390.js" id="asciicast-183390" async></script>