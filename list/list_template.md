# Linked List dengan fungsi push_back + Abstraksi Class dan 

## Prologue

Selain ada class, di C++ ada fitur yang bernama template, fitur ini memberikan fleksibilitas dalam penggunaan tipe, tanpa banyak mengubah *code* di *codebase*.

## Perubahan Kode

Sebelum inisialisasi *struct* dan *class* kita melakukan pendefinisian template, dengan apa yang ingin kita ubah.
```c++
template <class T>
```

Selain itu, karena tipenya fleksibel, maka untuk menghindari penggunaan variabel *temporary* saat penginputan, kita akan mengubah penginputan terletak di dalam class (di mana tipenya sudah sesuai).

## Kode

```c++
#include <iostream>
#include <memory>

// Deklarasi bahwa ini adalah template
// T di sini digunakan untuk menggantikan typename
template <class T>
struct node {
    // tipe data akan sesuai dengan yang kita inginkan
    T data;
    std::shared_ptr<node<T>> next;
};

template <class T>
class list {
  private:
    // Lihat bahwa menggunakan node<T> bukan node saja
    // hal tersebut karena kita harus memberitahu ke node, apa tipe data
    // yang ingin digunakan
    std::shared_ptr<node<T>> head, tail;
  
  public:
    list(){
        head = nullptr;
        tail = nullptr;
    }

    // Proses input dilakukan di dalam class, karena kita tidak bisa
    // mendeduksi tipe yang akan digunakan saat di main
    void input(int limit){
        T temp;
        while(limit--){
            std::cin >> temp;
            push_back(temp);
        }
    }

    // Push back seperti sebelumnya
    // hanya saja typenamenya T
    void push_back(T data){
        auto unlinked_node = std::make_shared<node<T>>();

        unlinked_node->data = data;
        unlinked_node->next = nullptr;

        if(tail != nullptr){
            tail->next = unlinked_node;
            tail = unlinked_node;
        }
        else {
            head = unlinked_node;
            tail = unlinked_node;
        }
    }

    void print(){
        auto iter = head;
        while(iter != nullptr){
            std::cout << iter->data << " ";
            iter = iter->next;
        }
        std::cout << std::endl;
    }
};

int main(){
    int n;
    std::cin >> n;
    // Dalam hal ini kita mendeklarasikan list<int> tidak hanya list saja
    // di mana kita menginginkan list yang datanya bertipe int
    list<int> linked_list;

    // Proses input
    linked_list.input(n);

    linked_list.print();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183387.js" id="asciicast-183387" async></script>