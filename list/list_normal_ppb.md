# Linked List dengan fungsi pop_back

## Perubahan Kode

Dalam kode ini kita menambahkan fungsi pop_back yang berfungsi untuk menghapus item terakhir di list :

```c++
void pop_back(list * object){
    node * will_be_tail = object->head;

    // Cari node yang paling belakang
    while(will_be_tail->next != object->tail)
        will_be_tail = will_be_tail->next;

    // jadikan null
    will_be_tail->next = NULL;

    // lalu hapus
    delete object->tail;
    object->tail = will_be_tail;
}
```

## Kode

Secara umum keseluruhan kode akan menjadi

```c++
#include <iostream>

struct node {
    int data;
    node * next;
};

struct list {
    node *head, *tail;
};

// Inisialisasi object list baru
void init(list * object){
    object->head = NULL;
    object->tail = NULL;
}

void push_front(list * object, int data){
    node * unlinked_node = new node;

    unlinked_node->data = data;
    unlinked_node->next = NULL;

    // Insert di depan head yang sebelumnya
    if(object->head != NULL){
        unlinked_node->next = object->head;
        object->head = unlinked_node;
    }
    else {
        object->head = unlinked_node;
        object->tail = unlinked_node;
    }
}

void push_back(list * object, int data){
    node * unlinked_node = new node;

    unlinked_node->data = data;
    unlinked_node->next = NULL;

    // Jika list sudah ada isinya ke sini
    if(object->tail != NULL){
        object->tail->next = unlinked_node;
        object->tail = unlinked_node;
    }
    else {
        object->head = unlinked_node;
        object->tail = unlinked_node;
    }
}

// Mencetak isi list secara berurutan
void print(list * object){
    node * iter = object->head;
    while(iter != NULL){
        std::cout << iter->data << " ";
        iter = iter->next;
    }
    std::cout << std::endl;
}

void pop_back(list * object){
    node * will_be_tail = object->head;

    // Cari node yang paling belakang
    while(will_be_tail->next != object->tail)
        will_be_tail = will_be_tail->next;

    // jadikan null
    will_be_tail->next = NULL;

    // lalu hapus
    delete object->tail;
    object->tail = will_be_tail;
}

int main(){
    int n, temp_int;
    std::cin >> n;
    list linked_list;
    init(&linked_list);

    while(n--){
        std::cin >> temp_int;
        push_back(&linked_list, temp_int);
    }

    pop_back(&linked_list);

    print(&linked_list);
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183389.js" id="asciicast-183389" async></script>