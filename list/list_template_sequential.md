# Linked List Sequential Search

## Definisi

Dalam hal ini kita akan mencari nilai dari suatu node di dalam linked list, dengan menggunakan metode *sequential search*.

## Perubahan Kode

```c++
bool search(T value){
    auto now = root;

    while(now){
        if(now == value)
            return true;
        now = now->next;
    }

    return false;
}
```

## Kode

```c++
#include <iostream>
#include <memory>

// Deklarasi bahwa ini adalah template
// T di sini digunakan untuk menggantikan typename
template <class T>
struct node {
    // tipe data akan sesuai dengan yang kita inginkan
    T data;
    std::shared_ptr<node<T>> next;
};

template <class T>
class list {
  private:
    // Lihat bahwa menggunakan node<T> bukan node saja
    // hal tersebut karena kita harus memberitahu ke node, apa tipe data
    // yang ingin digunakan
    std::shared_ptr<node<T>> head, tail;
  
  public:
    list(){
        head = nullptr;
        tail = nullptr;
    }

    // Proses input dilakukan di dalam class, karena kita tidak bisa
    // mendeduksi tipe yang akan digunakan saat di main
    void input(int limit){
        T temp;
        while(limit--){
            std::cin >> temp;
            push_back(temp);
        }
    }

    // Push back seperti sebelumnya
    // hanya saja typenamenya T
    void push_back(T data){
        auto unlinked_node = std::make_shared<node<T>>();

        unlinked_node->data = data;
        unlinked_node->next = nullptr;

        if(tail != nullptr){
            tail->next = unlinked_node;
            tail = unlinked_node;
        }
        else {
            head = unlinked_node;
            tail = unlinked_node;
        }
    }

    bool search(T value){
        auto now = head;

        while(now){
            if(now->data == value)
                return true;
            now = now->next;
        }

        return false;
    }

    void print(){
        auto iter = head;
        while(iter != nullptr){
            std::cout << iter->data << " ";
            iter = iter->next;
        }
        std::cout << std::endl;
    }
};

int main(){
    int n;
    std::cin >> n;
    // Dalam hal ini kita mendeklarasikan list<int> tidak hanya list saja
    // di mana kita menginginkan list yang datanya bertipe int
    list<int> linked_list;

    // Proses input
    linked_list.input(n);

    int search_val;
    std::cout << "Search : ";
    std::cin >> search_val;

    if(linked_list.search(search_val))
        std::cout << "Found!" << std::endl;
    else
        std::cout << "Not Found!" << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/184361.js" id="asciicast-184361" async></script>