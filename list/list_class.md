# Linked List dengan fungsi push_back + Abstraksi Class

## Prologue

Dalam bahasa modern C++, di sini saya mencoba mengaplikasikan beberapa program saya dan nantinya akan dilakukan proses modernisasi dengan C++ Modern.

## Kode

```c++
#include <iostream>
// Penggunaan <memory> untuk memory management
// seperti pengalokasian pointer di C++ Modern
#include <memory>

struct node {
    int data;
    // Shared pointer merupakan salah satu fitur baru
    // yang dikenalkan pada C++11
    std::shared_ptr<node> next;
};

class list {
  private:
    std::shared_ptr<node> head, tail;
  
  public:
    list(){
        head = nullptr;
        tail = nullptr;
    }

    void push_back(int data){
        // Penggunaan auto merupakan upaya dari C++ compiler
        // secara otomatis menebak tipe data dari yang kita inginkan
        // Hal ini berbeda dengan dynamic variable yang ada di bahasa
        // pemrogaman tingkat tinggi seperti Python dan Javascript
        // Make shared artinya kita membuat pointer shared_ptr dari node
        auto unlinked_node = std::make_shared<node>();

        unlinked_node->data = data;
        unlinked_node->next = nullptr;

        if(tail != nullptr){
            tail->next = unlinked_node;
            tail = unlinked_node;
        }
        else {
            head = unlinked_node;
            tail = unlinked_node;
        }
    }

    // Cetak satu per satu
    void print(){
        auto iter = head;
        while(iter != nullptr){
            std::cout << iter->data << " ";
            iter = iter->next;
        }
        std::cout << std::endl;
    }
};

int main(){
    int n, temp_int;
    std::cin >> n;
    list linked_list;

    while(n--){
        std::cin >> temp_int;
        linked_list.push_back(temp_int);
    }

    linked_list.print();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183387.js" id="asciicast-183387" async></script>