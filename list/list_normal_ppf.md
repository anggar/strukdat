# Linked List dengan fungsi pop_front

## Perubahan Kode

Fungsi dari pop_front sendiri di sini adalah untuk menghapus node yang ada paling belakang list, penambahan kodenya yaitu

```c++
void pop_front(list * object){
    // Set head sebagai yang akan dihapus
    node * will_be_deleted= object->head;

    // Perbarui pointer head
    object->head = will_be_deleted->next;

    // Hapus head uang sebelumnya
    delete will_be_deleted;
}
```

## Kode

Secara umum keseluruhan kode akan menjadi

```c++
#include <iostream>

struct node {
    int data;
    node * next;
};

struct list {
    node *head, *tail;
};

// Inisialisasi object list baru
void init(list * object){
    object->head = NULL;
    object->tail = NULL;
}

void push_front(list * object, int data){
    node * unlinked_node = new node;

    unlinked_node->data = data;
    unlinked_node->next = NULL;

    // Insert di depan head yang sebelumnya
    if(object->head != NULL){
        unlinked_node->next = object->head;
        object->head = unlinked_node;
    }
    else {
        object->head = unlinked_node;
        object->tail = unlinked_node;
    }
}

void push_back(list * object, int data){
    node * unlinked_node = new node;

    unlinked_node->data = data;
    unlinked_node->next 
    // Jika list sudah ada isinya ke sini
    if(object->tail != NULL){
        object->tail->next = unlinked_node;
        object->tail = unlinked_node;
    }= NULL;

    else {
        object->head = unlinked_node;
        object->tail = unlinked_node;
    }
}

void pop_front(list * object){
    // Set head sebagai yang akan dihapus
    node * will_be_deleted= object->head;

    // Perbarui pointer head
    object->head = will_be_deleted->next;

    // Hapus head uang sebelumnya
    delete will_be_deleted;
}

// Mencetak isi list secara berurutan
void print(list * object){
    node * iter = object->head;
    while(iter != NULL){
        std::cout << iter->data << " ";
        iter = iter->next;
    }
    std::cout << std::endl;
}

int main(){
    int n, temp_int;
    std::cin >> n;
    list linked_list;
    init(&linked_list);

    while(n--){
        std::cin >> temp_int;
        push_back(&linked_list, temp_int);
    }

    pop_front(&linked_list);

    print(&linked_list);
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183390.js" id="asciicast-183390" async></script>