# Linked List dengan fungsi pop_front dengan abstraksi class dan template

## Perubahan Kode

Tidak ada perubahan di sini, sama seperti sebelumnya.

## Kode

Secara umum keseluruhan kode akan menjadi

```c++
#include <iostream>
#include <memory>

// Deklarasi bahwa ini adalah template
// T di sini digunakan untuk menggantikan typename
template <class T>
struct node {
    // tipe data akan sesuai dengan yang kita inginkan
    T data;
    std::shared_ptr<node<T>> next;
};

template <class T>
class list {
  private:
    // Lihat bahwa menggunakan node<T> bukan node saja
    // hal tersebut karena kita harus memberitahu ke node, apa tipe data
    // yang ingin digunakan
    std::shared_ptr<node<T>> head, tail;
  
  public:
    list(){
        head = nullptr;
        tail = nullptr;
    }

    // Proses input dilakukan di dalam class, karena kita tidak bisa
    // mendeduksi tipe yang akan digunakan saat di main
    void input(int limit){
        T temp;
        while(limit--){
            std::cin >> temp;
            // Dan tentu saja ini
            push_back(temp);
        }
    }

    void push_front(T data){
        // Di sini letak perubahannya
        auto unlinked_node = std::make_shared<node<T>>();

        unlinked_node->data = data;
        unlinked_node->next = nullptr;

        if(head != nullptr){
            unlinked_node->next = head;
            head = unlinked_node;
        }
        else {
            head = unlinked_node;
            tail = unlinked_node;
        }
    }

    void pop_front(){
        auto will_be_head = head->next;
        head.reset();
        head = will_be_head;
    }

    // Push back seperti sebelumnya
    // hanya saja typenamenya T
    void push_back(T data){
        auto unlinked_node = std::make_shared<node<T>>();

        unlinked_node->data = data;
        unlinked_node->next = nullptr;

        if(tail != nullptr){
            tail->next = unlinked_node;
            tail = unlinked_node;
        }
        else {
            head = unlinked_node;
            tail = unlinked_node;
        }
    }

    void print(){
        auto iter = head;
        while(iter != nullptr){
            std::cout << iter->data << " ";
            iter = iter->next;
        }
        std::cout << std::endl;
    }
};

int main(){
    int n;
    std::cin >> n;
    // Dalam hal ini kita mendeklarasikan list<int> tidak hanya list saja
    // di mana kita menginginkan list yang datanya bertipe int
    list<int> linked_list;

    // Proses input
    linked_list.input(n);

    linked_list.pop_front();

    linked_list.print();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183390.js" id="asciicast-183390" async></script>