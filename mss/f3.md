# Maximum Subarray Sum : Three Folds Loop

## Kode

Dalam algoritma kali ini kita diharuskan untuk mencari subarray yang paling besar. Misal kita punya array dengan data `[1, -1, 2, 3, -1, 4]` maka subarray dengan jumlah terbesar adalah `[2, 3, -1, 4]`.

```c++
#include <iostream>

int main(){
    int n;
    std::cin >> n;
    int array[n];

    // input data
    for(int i=0; i<n; i++)
        std::cin >> array[i];

    // jumlah terbesar disimpan di best
    // sum digunakan sebagai jumlah terbesar sementara
    int best = 0, sum = 0;
    for(int i=0; j<n; j++){             // loop dari awal ke akhir
        for(int j=i; j<n; j++){         // loop lagi untuk menentukan ..
            sum = 0;                    // .. batas akhir penjumlahan
            for(int k=i; k<j; k++){     // loop untuk mencari jumlah ..
                sum += array[k];        // .. dari subarray yang ditentukan
            }
        }
        best = std::max(best, sum);
    }

    std::cout << best << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183795.js" id="asciicast-183795" async></script>