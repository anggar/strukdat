# Multiplication

Perkalian dilakukan seperti cara kita melakukan perkalian di kertas, kali *multiplier* dengan masing-masing multiplicand, atur posisinya sehingga terbentuk seperti pola jajar genjang, lalu jumlahkan seperti biasa.

## Kode
```c++
#include <iostream>
#include <bitset>

#define n 4

auto half_adder(auto input, int carry){
    return std::pair<int, int>((input.first & input.second) | (input.first & carry) | (carry & input.second), input.first ^ input.second ^ carry);
}

auto full_adder(auto input){
    std::bitset<10> result = 0;
    std::pair<int, int> temp_binary(0, 0);

    for(int i=0; i<n+1; i++){
        temp_binary = half_adder(std::pair<int, int>(input.first[i], input.second[i]), temp_binary.first);
        result[i] = temp_binary.second;
    }

    return result;
}

auto multiplication(auto input){
    std::bitset<10> result = 0;

    for(int i=0; i<n; i++){
        if(input.second[i] == 1){
            // memanfaatkan fungsi full_adder sebelumnya
            result = full_adder(std::pair<std::bitset<10>, std::bitset<n>>(result, input.first));
        }

        // multiplier geser satu kali ke kiri tiap pindah digit
        // multiplicand
        input.first = input.first << 1;
    }

    return result;
}

int main(){
    std::pair<std::bitset<n>, std::bitset<n>> input;
    std::cin >> input.first >> input.second;

    auto result = multiplication(input);

    std::cout << input.first << " x " << input.second << std::endl;
    std::cout << "= " << result << std::endl; 
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183465.js" id="asciicast-183465" async></script>