# Booth's Algorithm for Multiplication

Dengan cara yang sedikit berbeda, kita dapat memperoleh nilai yang sama. Cara ini sendiri juga memerlukan proses recoding sebelum dilakukan operasinya. Sebagai referensi, bisa dilihat di [Booth's multiplication algoritm.](https://en.wikipedia.org/wiki/Booth%27s_multiplication_algorithm)

## Kode
```c++
#include <iostream>
#include <bitset>
#include <array>
#include <cstdio>
#include <typeinfo>

#define n 10

auto half_adder(auto input, int carry){
    return std::pair<int, int>((input.first & input.second) | (input.first & carry) | (carry & input.second), input.first ^ input.second ^ carry);
}

auto full_adder(auto input){
    std::bitset<10> result = 0;
    std::pair<int, int> temp_binary(0, 0);

    for(int i=0; i<n+1; i++){
        temp_binary = half_adder(std::pair<int, int>(input.first[i], input.second[i]), temp_binary.first);
        result[i] = temp_binary.second;
    }

    return result;
}

auto recoding(auto input){
    std::array<int, 10> result= {0};

    if(input[0] == 1) result[0] = -1;

    for(int i=1; i<10; i++){
        if(input[i] == 1 && input[i-1] == 0) result[i] = -1;
        if(input[i] == 0 && input[i-1] == 1) result[i] = 1;
    }

    return result;
}

auto two_complements(auto set){
    int i = 0;
    while (i < set.count() && set[i])
      {
         set[i] = 0;
         ++i;
      }

    if (i < set.count())
      set[i] = 1;

    return set;
}

auto multiplication(auto input){
    std::bitset<10> result = 0;

    for(int i=0; i<n; i++){
        if(input.second[i] == 1){
            result = full_adder(std::pair<std::bitset<10>, std::bitset<n>>(result, input.first));
        }

        input.first = input.first << 1;
    }

    return result;
}

auto booth_multiplication(auto input){
    std::bitset<10> result = 0;

    std::bitset<n> satu = 1;
    // Cara untuk memperoleh 2's complement dari multiplier
    // Yaitu dena=gan menambah 1's complement dan 1
    auto multiplier_complement = full_adder(std::pair<std::bitset<n>, std::bitset<n>>(~input.first, satu));
    // multiplicand di recode dengan metode "Skipping the ones"
    auto recoded = recoding(input.second);

    std::cout << typeid(multiplier_complement).name() << std::endl;
    std::cout << typeid(~input.first).name() << std::endl;

    std::cout << multiplier_complement << std::endl;

    for(auto i: recoded)
        std::cout << i << ' ';
    std::cout << std::endl;

    for(int i=0; i<n; i++){
        // Jika 1 di hitung yang input.first
        if(recoded[i] == 1){
            result = full_adder(std::pair<std::bitset<10>, std::bitset<n>>(result, input.first));
        }
        // Jika -1 di recoded hitung yang 2's complement dari multiplier
        if(recoded[i] == -1){
            result = full_adder(std::pair<std::bitset<10>, std::bitset<10>>(result, multiplier_complement));
        }

        input.first = input.first << 1;
        multiplier_complement = multiplier_complement << 1;
    }

    return result;
}

int main(){
    std::pair<std::bitset<n>, std::bitset<n>> input;
    std::cin >> input.first >> input.second;

    auto result = booth_multiplication(input);

    std::cout << input.first << " x " << input.second << std::endl;
    std::cout << "= " << result << std::endl; 

    result = multiplication(input);

    std::cout << input.first << " x " << input.second << std::endl;
    std::cout << "= " << result << std::endl; 
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183467.js" id="asciicast-183467" async></script>