# Half Adder

Half adder adalah rangkaian dengan 2 input di mana output yang dihasilkan adala penjumlahan dari kedua bilangan tersebut.

## Kode
```c++
#include <iostream>

std::pair<int, int> half_adder(std::pair<int, int> input){
    return std::pair<int, int>(input.first & input.second, input.first ^ input.second);
}

int main(){
    std::pair<int, int> input;
    std::cin >> input.first >> input.second;

    std::pair<int, int> result = half_adder(input);     // First as a carry, second as a result

    std::cout << input.first << " + " << input.second << std::endl;
    std::cout << "= " << result.first << result.second << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183462.js" id="asciicast-183462" async></script>