# Full Adder

Full Adder adalah teknik aritmetika biner yang digunakan untuk menghitung jumlah dua buah bilangan biner, cara yang digunakan yaitu menggunakan banyak rangkaian half adder didalamnya. Dalam hal ini rangkain half adder disimulasikan dengan fungsi *half_adder*.

## Kode
```c++
#include <iostream>
#include <bitset>

#define n 5

std::pair<int, int> half_adder(std::pair<int, int> input, int carry){
    return std::pair<int, int>((input.first & input.second) | (input.first & carry) | (carry & input.second), input.first ^ input.second ^ carry);
}

std::bitset<n+1> full_adder(std::bitset<n> first, std::bitset<n> second){
    std::bitset<n+1> result = 0;
    std::pair<int, int> temp_binary(0, 0);

    // digit per digit dari kanan ke kiri
    for(int i=0; i<n+1; i++){
        temp_binary = half_adder(std::pair<int, int>(first[i], second[i]), temp_binary.first);
        // temp_binary.first sebagai carry
        // temp_binary.second sebagai hasil di digit tersebut
        result[i] = temp_binary.second;
    }

    return result;
}

int main(){
    std::pair<std::bitset<n>, std::bitset<n>> input;
    std::cin >> input.first >> input.second;

    auto result = full_adder(input.first, input.second);

    std::cout << input.first << " + " << input.second << std::endl;
    std::cout << "= " << result << std::endl; 
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183463.js" id="asciicast-183463" async></script>