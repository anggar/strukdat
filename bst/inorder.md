# Binary Search Tree : In Order Traversal

## Definisi

Jika sebelumnya kita telah mengkaji metode traversal *preorder*, sekarang akan dilakukan metode lain yaitu in order traversal. 

## In Order Function

```c++
void printInorderRecur(auto now){
    if (now->left) printInorderRecur(now->left);   // Ke kiri (jika tidak null)
    std::cout << now->data << " ";            // Jika sudah ke kiri, cetak hasilnya
    if (now->right) printInorderRecur(now->right); // Lalu ke kanan (jika tidak null)
    // Habis? Balik lagi ke stack rekursi sebelumnya
}
```


## Kode

Keseluruhan kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(temp);
        }
    }

    // Fungsi bantuan yang dipanggil dari main()
    // dikarenakan root memiliki sifat private maka node root ...
    // ... tidak bisa di akses dari fungsi main
    // Oleh karena itu kita buat fungsi di bawah
    void printInOrder(){
        printInorder(root);
        std::cout << std::endl;
    }
    
    void printInorderRecur(auto now){
        if (now->left) printInorderRecur(now->left);   // Ke kiri (jika tidak null)
        std::cout << now->data << " ";            // Jika telah ke kiri cetak
        if (now->right) printInorderRecur(now->right); // Lalu ke kanan (jika tidak null)
        // Habis? Balik lagi ke stack rekursi sebelumnya
    }

    void insert(T value){
        auto temp = std::make_shared<node<T>>();

        temp = init_node(value);

        if(root == nullptr){
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            if(temp->data < now->data){
                if(now->left)
                    now = now->left;
                else {
                    now->left = temp;
                    break;
                }
            } else {
                if(now->right)
                    now = now->right;
                else{
                    now->right = temp;
                    break;
                }
            }
        }
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);

    tree.printInOrder();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183838.js" id="asciicast-183838" async></script>

### Visualisasi Tree yang di Input

![Graph 1](graph1.png)
![Graph 2](graph2.png)
![Graph 3](graph3.png)