# Binary Search Tree : Basic

## Definisi

*Binary Search Tree* adalah tipe *tree* di mana masing-masing nodenya maksimal memiliki 2 *child* dan nilai *child* kanan lebih kecil dari nilai nodenya serta nilai *child* kiri lebih besar dari nilai nodenya.

## Abstraksi Node

```c++
template <class T>
struct node {
    T data;                         // Nilai dari node
    std::shared_ptr<node> left;     // Menunjuk child kirinya
    std::shared_ptr<node> right;    // Menunjuk child kanannya
};
```

## Abstraksi Tree

```c++
template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;      // Tree mempunyai root dimana ...
                                        // ...root tidak memiliki parent node

  public:
    bst()
    {
        root = nullptr;
    }
};
```
## Rekaman Layar

Tidak ada rekaman layar untuk program ini karena hanya menjelaskan struktur data dasarnya.