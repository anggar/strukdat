# Binary Search Tree : Insert

## Definisi

Jika pada bab sebelumnya kita telah mengkaji struktur dasar dari sebuah *tree* maka di sini kita akan membuat fungsi untuk melakukan 

## Fungsi `init_node`

Berfungsi untuk mendeklarasikan node baru yang memiliki nilai tertentu. Fungsi ini mirip dengan *constructor* di class.

```c++
auto init_node(T value){            // Mengembalikan pointer dari node
    auto temp = std::make_shared<node<T>>();

    temp->data = value;
    temp->left  = nullptr;
    temp->right = nullptr;

    return temp;
}
```

## Fungsi `insert`

```c++
void insert(T value){
    auto temp = std::make_shared<node<T>>();

    temp = init_node(value);

    // Jika tree kosong-ditandai dengan root menunjuk nullptr-, maka nilai ...
    // ... tersebut menjadi milik root
    if(root == nullptr){
        root = temp;
        return;
    }

    auto now = root;

    // Iterasi untuk mencari posisi di tree yang tepat
    while(true){
        if(temp->data < now->data){     // Jika data lebih kecil dari node sekarang
            if(now->left)               // Selama masih ada item yang valid ...
                now = now->left;        // ... terus cari ke bawah
            else {                      // Jika tidak ...
                now->left = temp;       // ... jadikan temp sebagai node tsb 
                break;
            }
        } else {                        // Jika data lebih besar dari node sekarang
            if(now->right)              // Selama masih ada item yang valid
                now = now->right;       // ... terus cari ke bawah
            else{                       // Jika tidak ...
                now->right = temp;      // ... jadikan temp sebagai node tsb
                break;
            }
        }
    }
}
```

## Kode

Keseluruhan kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(temp);
        }
    }

    void insert(T value){
        auto temp = std::make_shared<node<T>>();

        temp = init_node(value);

        if(root == nullptr){
            std::cout << value << " taruh di root" << std::endl;
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            // Untuk melakukan printing saat insertion
            std::cout << value << ": dari " << now->data;
            if(temp->data < now->data){
                std::cout << " ke kiri  <-" << std::endl;
                if(now->left)
                    now = now->left;
                else {
                    std::cout << "taruh" << std::endl;
                    now->left = temp;
                    break;
                }
            } else {
                std::cout << " ke kanan ->" << std::endl;
                if(now->right)
                    now = now->right;
                else{
                    std::cout << "taruh" << std::endl;
                    now->right = temp;
                    break;
                }
            }
        }
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183811.js" id="asciicast-183811" async></script>