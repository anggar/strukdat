# Binary Search Tree : Level Order Traversal

## Definisi

Jika sebelumnya kita telah mengkaji metode traversal *postorder*, sekarang akan dilakukan metode lain yaitu level order traversal. Dalam level order traversal, kita melakukan pencarian dari masing-masing kedalaman rendah dahulu, lalu ke kedalaman yang lebih dalam.

## Level Order Function

Untuk melakukan level order, kita memerlukan bantuan tipe data lain. Tipe data tersebut adalah `queue`. Di mana tipe data ini digunakan untuk menyimpan *node* mana saja yang belum dikunjungi.

```c++
void printLevelOrderRecur(auto now){
    std::cout << now->data << " ";
    if(now->left)  unvisited_node.push(now->left);
    if(now->right) unvisited_node.push(now->right);
    unvisited_node.pop();

    printLevelOrder(unvisited_node.front());
}
```

## Kode

Keseluruhan kode

```c++
#include <iostream>
#include <memory>
#include <queue>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;
    std::queue<std::shared_ptr<node<T>>> unvisited_node;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(temp);
        }
    }

    // Fungsi bantuan yang dipanggil dari main()
    // dikarenakan root memiliki sifat private maka node root ...
    // ... tidak bisa di akses dari fungsi main
    // Oleh karena itu kita buat fungsi di bawah
    void printLevelOrder(){
        unvisited_node.push(root);
        printLevelOrderRecur(unvisited_node.front());
        std::cout << std::endl;
    }
    
    void printLevelOrderRecur(auto now){
        std::cout << now->data << " ";
        if(now->left)  unvisited_node.push(now->left);
        if(now->right) unvisited_node.push(now->right);
        if(!unvisited_node.empty())
            unvisited_node.pop();

        if(!unvisited_node.empty())
            printLevelOrderRecur(unvisited_node.front());
    }

    void insert(T value){
        auto temp = std::make_shared<node<T>>();

        temp = init_node(value);

        if(root == nullptr){
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            if(temp->data < now->data){
                if(now->left)
                    now = now->left;
                else {
                    now->left = temp;
                    break;
                }
            } else {
                if(now->right)
                    now = now->right;
                else{
                    now->right = temp;
                    break;
                }
            }
        }
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);

    tree.printLevelOrder();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183879.js" id="asciicast-183879" async></script>

### Visualisasi Tree yang di Input

![Graph 1](graph1.png)
![Graph 2](graph2.png)
![Graph 3](graph3.png)