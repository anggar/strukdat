# Binary Search Tree : Maximum Value with Iterative Approach

## Definisi

Dalam hal ini kita akan mencari nilai maksimal yang ada di sebuah *tree*.

## Find Max

```c++
int find_max(){
    now = root;


    while(now){
        if(now->right){
            now = now->right;
            continue;
        }
        else
            return now->data;
    }

    return -1;
}
```


## Kode

Keseluruhan kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(temp);
        }
    }

    int find_max(){
        now = root;


        while(now){
            if(now->right){
                now = now->right;
                continue;
            }
            else
                return now->data;
        }

        return -1;
    }

    void insert(T value){
        auto temp = std::make_shared<node<T>>();

        temp = init_node(value);

        if(root == nullptr){
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            if(temp->data < now->data){
                if(now->left)
                    now = now->left;
                else {
                    now->left = temp;
                    break;
                }
            } else {
                if(now->right)
                    now = now->right;
                else{
                    now->right = temp;
                    break;
                }
            }
        }
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);

    tree.find_max();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/184606.js" id="asciicast-184606" async></script>

### Visualisasi Tree yang di input

![Graph 1](graph1.png)