# Binary Search Tree : Pre Order Traversal

## Definisi

Jika pada bab sebelumnya kita telah mengkaji struktur dasar dari sebuah *tree* maka di sini kita akan membuat fungsi untuk melakukan *tree traversal* dengan metode preorder.

## Pre Order Function

```c++
void printPreOrderRecur(auto now){
    std::cout << now->data << " ";                  // Mencetak datanya dahulu
    if (now->left) printPreOrderRecur(now->left);   // Lalu ke kiri (jika tidak null)
    if (now->right) printPreOrderRecur(now->right); // Lalu ke kanan (jika tidak null)
    // Habis? Balik lagi ke stack rekursi sebelumnya
}
```


## Kode

Keseluruhan kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(temp);
        }
    }

    // Fungsi bantuan yang dipanggil dari main()
    // dikarenakan root memiliki sifat private maka node root ...
    // ... tidak bisa di akses dari fungsi main
    // Oleh karena itu kita buat fungsi di bawah
    void printPreOrder(){
        printPreOrderRecur(root);
        std::cout << std::endl;
    }
    
    void printPreOrderRecur(auto now){
        std::cout << now->data << " ";                  // Mencetak datanya dahulu
        if (now->left) printPreOrderRecur(now->left);   // Lalu ke kiri (jika tidak null)
        if (now->right) printPreOrderRecur(now->right); // Lalu ke kanan (jika tidak null)
        // Habis? Balik lagi ke stack rekursi sebelumnya
    }

    void insert(T value){
        auto temp = std::make_shared<node<T>>();

        temp = init_node(value);

        if(root == nullptr){
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            if(temp->data < now->data){
                if(now->left)
                    now = now->left;
                else {
                    now->left = temp;
                    break;
                }
            } else {
                if(now->right)
                    now = now->right;
                else{
                    now->right = temp;
                    break;
                }
            }
        }
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);

    tree.printPreOrder();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183834.js" id="asciicast-183834" async></script>

### Visualisasi Tree yang di input

![Graph 1](graph1.png)
![Graph 2](graph2.png)
![Graph 3](graph3.png)