# Binary Search Tree : Minimum Value

## Definisi

Dalam hal ini kita akan mencari nilai minimal yang ada di sebuah *tree*.

## Find Min

```c++
int find_min_recur(auto now){
   if(now->left)
        return find_min_recur(now->left);
    else
        return now->data;

    return -1;
}
```


## Kode


```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(temp);
        }
    }

    int find_min(){
        return find_min_recur(root);
    }

    int find_min_recur(auto now){
       if(now->left)
            return find_min_recur(now->left);
        else
            return now->data;

        return -1;
    }

    void insert(T value){
        auto temp = std::make_shared<node<T>>();

        temp = init_node(value);

        if(root == nullptr){
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            if(temp->data < now->data){
                if(now->left)
                    now = now->left;
                else {
                    now->left = temp;
                    break;
                }
            } else {
                if(now->right)
                    now = now->right;
                else{
                    now->right = temp;
                    break;
                }
            }
        }
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);

    tree.find_min();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/184607.js" id="asciicast-184607" async></script>

### Visualisasi Tree yang di input

![Graph 1](graph1.png)