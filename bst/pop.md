# Binary Search Tree : Pop

## Definisi

Fungsi `pop` adalah fungsi yang digunakan untuk menghapus *node* di sebuah tree.

## Cara Dasar

Cara yang akan dilakukan yaitu dengan mengambil *subtree* di sebelah kanan dan kiri node yang akan dihapus, lalu hapus node yang ingin kita inginkan. Setelah itu masukkan kembali *subtree* tersebut ke bagian *tree*, sehingga hanya satu node saja yang terhapus.

## Perubahan Kode

### Modifikasi Fungsi `insert`

Dari cara di atas, jelas kita harus melakukan *insertion* lagi setelah menghapus node tertentu. Oleh karena itu, kita harus memodifikasi sedikit fungsi dari `insert` sehingga bisa melakukan *insertion* berdasarkan node (yang sudah memiliki member *left* dan *right*) daripada berdasarkan value saja (*left* dan *right* null).

Untuk itu fungsi kita rubah dari
```c++
void insert(T value){
    auto temp = std::make_shared<node<T>>();

    temp = init_node(value);        // Langsung kita buat node baru

    if(root == nullptr){
        .
        .
        .
}
```

menjadi

```c++
void insert(auto base, T value){
    auto temp = std::make_shared<node<T>>();

    if(base == nullptr)
        temp = init_node(value);    // Jika base kosong maka buat node baru
    else
        temp = base;                // Jika base ada nilainya, pakai base

    if(root == nullptr){
        .
        .
        .
}
```

Selain memodifikasi fungsi `insert`, kita juga harus membuat fungsi baru yaitu fungsi `pop`.

### Fungsi `pop`

```c++
void pop(T value){               // Pop node dengan nilai value
    auto now = root;             // Digunakan untuk mencari node yg sesuai
    std::shared_ptr<node<T>> tr; // Digunakan unutk menyimpan subtree kanan
    std::shared_ptr<node<T>> tl; // Digunakan untuk menyimpan subtree kiri

    if(value == root->data){     // Jika node yang kita cari ada di root
        tr = now->right;         // Simpan subtree kanan
        tl = now->left;          // Simpan subtree kiri
        root = nullptr;          // Reset nilai root
        if(tl != nullptr) insert(tl, NULL); // Insertion ulang subtree kiri
        if(tr != nullptr) insert(tr, NULL); // Insertion ulang subtree kanan
        return;
    }

    while(now){                 // Jika node tidak berada di root
        if(now->left!=nullptr && (value == now->left->data)){ // Jika nilai ada ...
            tr = now->left->right; // Simpan subtree kanan     // di sebelah kiri
            tl = now->left->left;  // Simpan subtree kiri
            now->left = nullptr;   // Reset node yang ingin dihapus
            if(tl != nullptr) insert(tl, NULL); // Insertion ulang subtree kiri  
            if(tr != nullptr) insert(tr, NULL); // Insertion ulang subtree kanan 
            return;
        }

        // Jika nilai ada di kanan
        if(now->right!=nullptr && (value == now->right->data)){
            tr = now->right->right; // Simpan subtree kanan
            tl = now->right->left;  // Simpan subtree kiri
            now->right = nullptr;
            if(tr != nullptr) insert(tr, NULL); // Insertion ulang subtree kanan 
            if(tl != nullptr) insert(tl, NULL); // Insertion ulang subtree kiri 
            return;
        }

        if(value < now->data){  // Jika belum ketemu, traverse terus
            now = now->left;
        } else {
            now = now->right;
        }
    }
}
```

## Kode

Keseluruhan kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(nullptr, temp);
        }
    }

    // Fungsi bantuan yang dipanggil dari main()
    // dikarenakan root memiliki sifat private maka node root ...
    // ... tidak bisa di akses dari fungsi main
    // Oleh karena itu kita buat fungsi di bawah
    void printInOrder(){
        printInOrderRecur(root);
        std::cout << std::endl;
    }
    
    void printInOrderRecur(auto now){
        std::cout << now->data << " ";                  // Mencetak datanya dahulu
        if (now->left) printInOrderRecur(now->left);   // Lalu ke kiri (jika tidak null)
        if (now->right) printInOrderRecur(now->right); // Lalu ke kanan (jika tidak null)
    }

    void insert(auto base, T value){
        auto temp = std::make_shared<node<T>>();

        if(base == nullptr)
            temp = init_node(value);    // Jika base kosong maka buat node baru
        else
            temp = base;                // Jika base ada nilainya, pakai base

        if(root == nullptr){
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            if(temp->data < now->data){
                if(now->left)
                    now = now->left;
                else {
                    now->left = temp;
                    break;
                }
            } else {
                if(now->right)
                    now = now->right;
                else{
                    now->right = temp;
                    break;
                }
            }
        }
    }

    void pop(T value){               // Pop node dengan nilai value
        auto now = root;             // Digunakan untuk mencari node yg sesuai
        std::shared_ptr<node<T>> tr; // Digunakan unutk menyimpan subtree kanan
        std::shared_ptr<node<T>> tl; // Digunakan untuk menyimpan subtree kiri

        if(value == root->data){     // Jika node yang kita cari ada di root
            tr = now->right;         // Simpan subtree kanan
            tl = now->left;          // Simpan subtree kiri
            root = nullptr;          // Reset nilai root
            if(tl != nullptr) insert(tl, NULL); // Insertion ulang subtree kiri
            if(tr != nullptr) insert(tr, NULL); // Insertion ulang subtree kanan
            return;
        }

        while(now){                 // Jika node tidak berada di root
            if(now->left!=nullptr && (value == now->left->data)){ // Jika nilai ada ...
                tr = now->left->right; // Simpan subtree kanan     // di sebelah kiri
                tl = now->left->left;  // Simpan subtree kiri
                now->left = nullptr;   // Reset node yang ingin dihapus
                if(tl != nullptr) insert(tl, NULL); // Insertion ulang subtree kiri  
                if(tr != nullptr) insert(tr, NULL); // Insertion ulang subtree kanan 
                return;
            }

            // Jika nilai ada di kanan
            if(now->right!=nullptr && (value == now->right->data)){
                tr = now->right->right; // Simpan subtree kanan
                tl = now->right->left;  // Simpan subtree kiri
                now->right = nullptr;
                if(tr != nullptr) insert(tr, NULL); // Insertion ulang subtree kanan 
                if(tl != nullptr) insert(tl, NULL); // Insertion ulang subtree kiri 
                return;
            }

            if(value < now->data){  // Jika belum ketemu, traverse terus
                now = now->left;
            } else {
                now = now->right;
            }
        }
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);

    int popped;
    std::cout << "Pop \t: ";
    std::cin >> popped;
    
    std::cout << "Before \t: ";
    tree.printInOrder();

    tree.pop(popped);

    std::cout << "After \t: ";
    tree.printInOrder();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183846.js" id="asciicast-183846" async></script>

### Visualisasi Tree yang di input

![Graph 3](graph3.png)
![Graph 4](graph4.png)
![Graph 5](graph5.png)
![Graph 6](graph6.png)