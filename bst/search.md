# Binary Search Tree : Search

## Definisi

Dalam hal ini kita akan mencari nilai dari suatu node di dalam tree, apakah ada atau tidak?

## Binary Search

```c++
int search(auto now, T value){
    if(now == nullptr) return 0;

    if(value == now->data) return 1;
    else if(value < now->data) return 0 + now(now->left, value);
    else if(value > now->data) return 0 + now(now->right, value);
}
```

## Kode

Keseluruhan kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(temp);
        }
    }

    bool search(T value){
        return search_r(root, value);
    }

     int search_r(auto now, T value){
        if(now == nullptr) return 0;

        if(value == now->data) return 1;
        else if(value < now->data) return 0 + search_r(now->left, value);
        else if(value > now->data) return 0 + search_r(now->right, value);
    }

    void insert(T value){
        auto temp = std::make_shared<node<T>>();

        temp = init_node(value);

        if(root == nullptr){
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            if(temp->data < now->data){
                if(now->left)
                    now = now->left;
                else {
                    now->left = temp;
                    break;
                }
            } else {
                if(now->right)
                    now = now->right;
                else{
                    now->right = temp;
                    break;
                }
            }
        }
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);

    int search_val;
    std::cout << "Search : ";
    std::cin >> search_val;

    if(tree.search(search_val))
        std::cout << "Found!" << std::endl;
    else
        std::cout << "Not Found!" << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/184360.js" id="asciicast-184360" async></script>

### Visualisasi Tree yang di input

![Graph 1](graph1.png)