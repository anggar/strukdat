# Binary Search Tree : Subtree

## Definisi

Fungsi `subtree` adalah fungsi yang digunakan untuk mengembalikan sebuah objek `bst` baru yang merupakan sebuah *subtree* dari tree lainnya.

## Perubahan Kode

Cara yang dapat dilakukan yaitu dengan membuat tree baru, lalu kita *insert* subtree tersebut ke tree yang baru, sehingga node dari *subtree* akan menjadi `root` di tree yang baru.

### Modifikasi Fungsi `insert`

Untuk dapat melakukan cara di atas, kita harus melakukan *insertion* ke tree yang baru. Oleh karena itu, kita harus memodifikasi sedikit fungsi dari `insert` sehingga bisa melakukan *insertion* berdasarkan node (yang sudah memiliki member *left* dan *right*) daripada berdasarkan value saja (*left* dan *right* null).

Untuk itu fungsi kita rubah dari
```c++
void insert(T value){
    auto temp = std::make_shared<node<T>>();

    temp = init_node(value);        // Langsung kita buat node baru

    if(root == nullptr){
        .
        .
        .
}
```

menjadi

```c++
void insert(auto base, T value){
    auto temp = std::make_shared<node<T>>();

    if(base == nullptr)
        temp = init_node(value);    // Jika base kosong maka buat node baru
    else
        temp = base;                // Jika base ada nilainya, pakai base

    if(root == nullptr){
        .
        .
        .
}
```

### Fungsi `subtree`

Fungsi ini cukup unik karena dalam fungsi ini kita akan mengembalikan *tree* baru, di mana tree tersebut adalah *subtree* dengan node yang kita inginkan, dan *children* adalah semua node di bawahnya. Yang menjadikan ini unik, adalah implementasinya dilakukan di dalam class *bst* sendiri.

```c++
auto subtree(T value){
    auto now = root;            // Untuk traversal tree

    bst<int> new_tree;          // Kita buat tree baru

    while(now){
        if(value == now->data){         // Jika ditemukan
            new_tree.insert(now, NULL); // tree yang baru kita insert node now
            return new_tree;            // Kembalikan tree new_tree
        }

        else if(value < now->data)
            now = now->left;
        else if(value > now->data)
            now = now->right;
    }

    new_tree.insert(root, NULL);        // Jika tidak ditemukan ya tree awal saja
    return new_tree;
}
``` 

## Kode

Keseluruhan kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class bst
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    bst()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    // Ini merupakan fungsi bantuan untuk melakukan penginputan
    // Karena menggunakan template, yang mengakibatkan tipe data bisa fleksibel
    // maka penginputan dilakukan di dalam class bst
    void input(int limit){      // limit merupakan banyaknya data yang akan diinput
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(nullptr, temp);
        }
    }

    // Fungsi bantuan yang dipanggil dari main()
    // dikarenakan root memiliki sifat private maka node root ...
    // ... tidak bisa di akses dari fungsi main
    // Oleh karena itu kita buat fungsi di bawah
    void printInOrder(){
        printInOrderRecur(root);
        std::cout << std::endl;
    }
    
    void printInOrderRecur(auto now){
        std::cout << now->data << " ";                  // Mencetak datanya dahulu
        if (now->left) printInOrderRecur(now->left);   // Lalu ke kiri (jika tidak null)
        if (now->right) printInOrderRecur(now->right); // Lalu ke kanan (jika tidak null)
    }

    void insert(auto base, T value){
        auto temp = std::make_shared<node<T>>();

        if(base == nullptr)
            temp = init_node(value);    // Jika base kosong maka buat node baru
        else
            temp = base;                // Jika base ada nilainya, pakai base

        if(root == nullptr){
            root = temp;
            return;
        }

        auto now = root;

        while(true){
            if(temp->data < now->data){
                if(now->left)
                    now = now->left;
                else {
                    now->left = temp;
                    break;
                }
            } else {
                if(now->right)
                    now = now->right;
                else{
                    now->right = temp;
                    break;
                }
            }
        }
    }

    auto subtree(T value){
        auto now = root;            // Untuk traversal tree

        bst<int> new_tree;          // Kita buat tree baru

        while(now){
            if(value == now->data){         // Jika ditemukan
                new_tree.insert(now, NULL); // tree yang baru kita insert node now
                return new_tree;            // Kembalikan tree new_tree
            }

            else if(value < now->data)
                now = now->left;
            else if(value > now->data)
                now = now->right;
        }

        new_tree.insert(root, NULL);        // Jika tidak ditemukan ya tree awal saja
        return new_tree;
    }
};

int main(){
    int n;
    std::cin >> n;

    bst<int> tree;
    tree.input(n);

    int subtree;
    std::cout << "Subtree : ";
    std::cin >> subtree;
    
    std::cout << "Before \t: ";
    tree.printInOrder();

    auto sub = tree.subtree(subtree);

    std::cout << "After \t: ";
    sub.printInOrder();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183880.js" id="asciicast-183880" async></script>

### Visualisasi Tree yang di input

![Graph 2](graph2.png)
![Graph 3](graph3.png)