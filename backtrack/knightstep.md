# Minimum Knight Steps

## Prologue

Mencari langkah minimal yang diperlukan untuk sebuah bidak kuda berpindah dari satu posisi ke posisi lain dalam papan catur.

## Kode

```c++
#include <iostream>
#include <queue>
#include <list>

#define n 8

#define ull unsigned long long

struct node
{
    int x, y, h;
    node *parent;       // Pointer yang menunjuk parent atas 
    node *la, *lb, *lc, *ld, *le, *lf, *lg, *lh;
};

namespace pretty_print {
    void data(int x){
        for(int i=0; i<n; i++){
            std::cout << char(179) << " ";

            if(i == x) std::cout << char(254) << " ";
            else std::cout << "  ";
        }
        std::cout << char(179) << std::endl; 
    }

    void header(){
        for(int i=0; i<n+1; i++){
            if(i == 0)  std::cout << char(218);
            else if(i == n)  std::cout << char(191);
            else std::cout << char(194);

            if(i != n) for(int j=0; j<3; j++) std::cout << char(196);
        }
        std::cout << std::endl;
    }

    void middle(){
        for(int i=0; i<n+1; i++){
            if(i == 0)  std::cout << char(195);
            else if(i == n)  std::cout << char(180);
            else std::cout << char(197);

            if(i != n) for(int j=0; j<3; j++) std::cout << char(196);
        }
        std::cout << std::endl;
    }

    void footer(){
        for(int i=0; i<n+1; i++){
            if(i == 0)  std::cout << char(192);
            else if(i == n)  std::cout << char(217);
            else std::cout << char(193);

            if(i != n) for(int j=0; j<3; j++) std::cout << char(196);
        }
        std::cout << std::endl;
    }
};

class list
{
  private:
    node *head, *tail;
    std::queue<node *> langkah;
    node *yang_dicari;

    void melangkah(node * now, int xo, int yo){
        int x = now->x;
        int y = now->y;

        if(now->x == xo && now->y == yo){
            std::cout << "Ada " << now->h << " langkah minimal yang perlu dilakukan." << std::endl;
            yang_dicari = now;
            return;
        }

        if(x+1 <= n && y+2 <=n) now->la = init_node(x+1, y+2, now->h + 1, now);
        else now->la = init_node(now->x, now->y, now->h + 1, now);

        if(x+2 <= n && y+1 <=n) now->lb = init_node(x+2, y+1, now->h + 1, now);
        else now->lb = init_node(now->x, now->y, now->h + 1, now);
        
        if(x+2 <= n && y-1 > 0) now->lc = init_node(x+2, y-1, now->h + 1, now);
        else now->lc = init_node(now->x, now->y, now->h + 1, now);
        
        if(x+1 <= n && y-2 > 0) now->ld = init_node(x+1, y-2, now->h + 1, now);
        else now->ld = init_node(now->x, now->y, now->h + 1, now);
        
        if(x-1 >  0 && y-2 > 0) now->le = init_node(x-1, y-2, now->h + 1, now);
        else now->le = init_node(now->x, now->y, now->h + 1, now);
        
        if(x-2 >  0 && y-1 > 0) now->lf = init_node(x-2, y-1, now->h + 1, now);
        else now->lf = init_node(now->x, now->y, now->h + 1, now);
        
        if(x-2 >  0 && y+1 <=n) now->lg = init_node(x-2, y+1, now->h + 1, now);
        else now->lg = init_node(now->x, now->y, now->h + 1, now);
        
        if(x-1 >  0 && y+2 <=n) now->lh = init_node(x-1, y+2, now->h + 1, now);
        else now->lh = init_node(now->x, now->y, now->h + 1, now);

        langkah.push(now->la);
        langkah.push(now->lb);
        langkah.push(now->lc);
        langkah.push(now->ld);
        langkah.push(now->le);
        langkah.push(now->lf);
        langkah.push(now->lg);
        langkah.push(now->lh);

        langkah.pop();
        melangkah(langkah.front(), xo, yo);
    }

  public:
    list()
    {
        head = NULL;
        tail = NULL;
    }
    node * init_node(int xa, int ya, int l, node * from){
        node * rt = new node;
        rt->parent = from;
        rt->x = xa;
        rt->y = ya;
        rt->h = l;
        rt->la = NULL;
        rt->lb = NULL;
        rt->lc = NULL;
        rt->ld = NULL;
        rt->le = NULL;
        rt->lf = NULL;
        rt->lg = NULL;
        rt->lh = NULL;

        return rt;
    }
    void push_back(int x, int y)
    {
        node *temp = init_node(x, y, 0, NULL);
        if (head == NULL)
        {
            head = temp;
            tail = temp;
            temp = NULL;
        }
    }

    void print_langkah(){
        auto now = yang_dicari;
        int count = 0;
        std::list<std::pair<int,int>> steps;
        while(now){
            steps.push_front(std::pair<int,int>(now->x,now->y));
            now = now->parent;
        }

        getchar(); // '\n' handling

        for(auto step: steps){
            std::cout << "Langkah #" << count++ << std::endl;
            pretty_print::header();
            for(int i=1; i<=n; i++){
                if(i==step.second) pretty_print::data(step.first-1);
                else pretty_print::data(-1);     // Impossible
                if(i!=n) pretty_print::middle();
            }
            pretty_print::footer();
            std::cout << std::endl;

            getchar();
        }
    }

    // Karena dari fungsi main tidak bisa mengakses variabel head
    // Maka kita buat fungsi yang menjembataninya
    void awal_langkah(int xo, int yo){
        langkah.push(head);
        melangkah(head, xo, yo);
    }
};

int main()
{
    int xa, ya, xo, yo;
    std::cin >> xa >> ya >> xo >> yo;

    list kemungkinan;
    kemungkinan.push_back(xa, ya);

    kemungkinan.awal_langkah(xo, yo);

    kemungkinan.print_langkah();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183788.js" id="asciicast-183788" async></script>