# N Rook

## Prologue

Mencari semua kemungkinan posisi dari *n* benteng di dalam papan catur berukuran nxn sehingga semua benteng tersebut dalam posisi yang aman.

## Perubahan Kode

Dibandingkan dengan kode di [N Pawn](backtrack/npawn.md), ada perubahan di struktur data dari *tree*. Karena ini merupakan benteng maka kita perlu menandai kolom mana sajakah yang nantinya tidak boleh ditempati di saat melakukan proses pencarian. Sehingga kita membuat array *taken[n]* untuk hal tersebut.

Selain itu, kita juga perlu melakukan sedikit modifikasi di fungsi *simulate* yang subbagian else, yaitu 

```c++
void simulate(...){
    .
    .
    else {
        for(...){
            if(taken[i] == 0){
                taken[i] = 1;
                base->next[i] = init_node(base, std::pair<int, int>(step, i));
                simulate(step+1, base->next[i]);
                taken[i] = 0;
            }
        }
    }
}
```

## Kode

```c++
#include <iostream>
#include <queue>
#include <memory>

#define n 4

struct node {
    std::pair<int, int> data;       // Data pair yang berupa koordinat benteng
    std::shared_ptr<node> parent;   // Parent dari node tersebut di tree
    std::shared_ptr<node> next[n];  // Children tree tersebut
};

// Penggunaan namespace pretty_print dimaksudkan agar tidak mengganggu
// class tree dari segi struktur kode
namespace pretty_print {
    void data(int x){
        for(int i=0; i<n; i++){
            std::cout << char(179) << " ";

            if(i == x) std::cout << char(254) << " ";
            else std::cout << "  ";
        }
        std::cout << char(179) << std::endl; 
    }

    void header(){
        for(int i=0; i<n+1; i++){
            if(i == 0)  std::cout << char(218);
            else if(i == n)  std::cout << char(191);
            else std::cout << char(194);

            if(i != n) for(int j=0; j<3; j++) std::cout << char(196);
        }
        std::cout << std::endl;
    }

    void middle(){
        for(int i=0; i<n+1; i++){
            if(i == 0)  std::cout << char(195);
            else if(i == n)  std::cout << char(180);
            else std::cout << char(197);

            if(i != n) for(int j=0; j<3; j++) std::cout << char(196);
        }
        std::cout << std::endl;
    }

    void footer(){
        for(int i=0; i<n+1; i++){
            if(i == 0)  std::cout << char(192);
            else if(i == n)  std::cout << char(217);
            else std::cout << char(193);

            if(i != n) for(int j=0; j<3; j++) std::cout << char(196);
        }
        std::cout << std::endl;
    }
};

class possibilities_tree
{
  private:
    std::shared_ptr<node> root;
    int possibilities = 0;
    int taken[n] = {0};

    // fungsi melakukan simulasi, tidak bisa di akses dari main
    void simulate(int step, auto base){
        if(step == n){
            // Jika benar maka print
            auto now = base;
            possibilities++;
            std::cout << "Kemungkinan #" << possibilities << "\n\n";
            pretty_print::header();
            // Cetak dari bawah ke atas
            while(now->parent){
                pretty_print::data(now->data.second);
                now = now->parent;
                if(now->parent) pretty_print::middle();
            }
            pretty_print::footer();
            std::cout << std::endl;

            getchar();

            return;
        }
        else {
            for(int i=0; i<n; i++){
                if(taken[i] == 0){
                    taken[i] = 1;
                    base->next[i] = init_node(base, std::pair<int, int>(step, i));
                    simulate(step+1, base->next[i]);
                    taken[i] = 0;
                }
            }
        }
    }

  public:
    possibilities_tree()
    {
        root = std::make_shared<node>();
        root->data = std::pair<int, int>(0, 0);
        root->parent = nullptr;
        for(int i=0; i<n; i++)
            root->next[i] = nullptr;
    }

    // fungsi yang tertrigger dari main
    void sim(){
        simulate(0, root);
    }

    // deklarasi untuk membuat node baru
    auto init_node(std::shared_ptr<node> from, std::pair<int, int> value){
        auto temp = std::make_shared<node>();

        temp->data = value;
        temp->parent = from;
        for(int i=0; i<n; i++)
            temp->next[i] = nullptr;

        return temp;
    }
};

int main(){
    possibilities_tree tree;
    tree.sim();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183790.js" id="asciicast-183790" async></script>