# AVL Tree : Input and Balancing

## Definisi

Karena *AVL Tree* sendiri merupakan *self-balancing tree*, maka kita perlu suatu indikator untuk mengetahui apakah suatu tree sudah seimbang. Indikator tersebut bernama *balance factor*. Balance factor suatu tree artinya tinggi *subtree* kiri dikurangi tinggi *subtree* kanan. Contohnya, untuk *tree* dibawah memiliki *balance factor* 0.

![Graph 1](graph1.png)

Sedangkan *tree* di bawah memiliki *balance factor* 1;

![Graph 5](graph5.png)


## Implementasi

Untuk mencari *balance factor* dari suatu *subtree*, kita dapat membuat fungsi `get_balance`

```c++
int get_balance(auto base)
{
    return height(base->left) - height(base->right);
}
```

Sedangkan untuk proses inputnya sendiri, karena ini merupakan *self-balancing tree*, maka setiap kali kita melakukan input, kita harus melakukan proses *balancing*. 

```c++
    void input(int limit){
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(nullptr, temp);
        }
    }
```

### Perubahan fungsi `insert`

```c++
void insert(T value){
    root = insert_r(value);
}

auto insert_r(auto now, T value){
    auto temp = std::make_shared<node<T>>();

    if(now == nullptr){
        temp = init_node(value);
    }
    else
        temp = now;

    if(now == nullptr)
        return temp;

    if(temp->data < now->data)
        temp = insert_r(now->left, value);
    else
        temp = insert_r(now->right, value);

    return temp;

    balancing();
}
```


### Fungsi `balancing`
```c++
void balancing(){
    auto now = root;

    while(now){
        int balance = get_balance(now);
      
        printf("%d\n", balance);

        if(balance == 0)
            break;

        // Left Left Case
        if (balance > 1 && now->data < now->left->data){
            rotate_right(now);
            now = now->left;
        }
     
        // Right Right Case
        if (balance < -1 && now->data > now->right->data){
            rotate_left(now);
            now = now->right;
        }
     
        // Left Right Case
        if (balance > 1 && now->data > now->left->data)
        {
            now->left =  rotate_left(now->left);
            rotate_right(now);
            now = now->left;            
        }
     
        // Right Left Case
        if (balance < -1 && now->data < now->right->data)
        {
            now->right = rotate_right(now->right);
            rotate_left(now);
            now = now->right;
        }
    }
}
```

Di dalam fungsi `balancing` sendiri memanggil dua fungsi yang belum kita ketahui sebelumnya. Fungsi tersebut adalah `rotate_left` dan `rotate_right`. 

### Rotate Left

Fungsi ini berguna untuk "memutar" posisi dari suatu node, sehingga beberapa node akan bertukar posisinya.

![We Shall Rotate to Left!](http://www.cs.uah.edu/~rcoleman/CS221/Trees/Images/AVL02.jpg)

```c++
auto rotate_left(auto base)
{
    auto hold = base->right;
    
    base->right = hold->left;
    hold->left = base;
    
    fix_height(base);
    fix_height(hold);

    return hold;
}
```

### Rotate Right

Fungsi ini berguna untuk "memutar" posisi dari suatu node, sehingga beberapa node akan bertukar posisinya.

![We Shall Rotate to Right!](http://www.cs.uah.edu/~rcoleman/CS221/Trees/Images/AVL01.jpg)

```c++
auto rotate_right(auto base)
{
    auto hold = base->left;
    
    base->left = hold->right;
    hold->right = base;
    
    fix_height(base);
    fix_height(hold);
    
    return hold;
}
```


## Kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    int height;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class avl
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    avl()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->height = 0;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    int height(auto base)
    {
        return base?base->height: 0;
    }   

    void fix_height(auto base)
    {
        base->height = (height(base->left) > height(base->right)?(height(base->left) + 1):(height(base->right) + 1));
    }

    void input(int limit){
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(nullptr, temp);
        }
    }

    void balancing(auto base){
        auto now = base;

        while(now){
            int balance = get_balance(now);
          
            printf("%d\n", balance);

            if(balance == 0)
                break;

            // Left Left Case
            if (balance > 1 && now->data < now->left->data){
                rotate_right(now);
                now = now->left;
            }
         
            // Right Right Case
            if (balance < -1 && now->data > now->right->data){
                rotate_left(now);
                now = now->right;
            }
         
            // Left Right Case
            if (balance > 1 && now->data > now->left->data)
            {
                now->left =  rotate_left(now->left);
                rotate_right(now);
                now = now->left;            
            }
         
            // Right Left Case
            if (balance < -1 && now->data < now->right->data)
            {
                now->right = rotate_right(now->right);
                rotate_left(now);
                now = now->right;
            }
        }
    }

    void insert(T value){
        root = insert_r(value);
    }

    auto insert_r(auto now, T value){
        auto temp = std::make_shared<node<T>>();

        if(now == nullptr){
            temp = init_node(value);
        }
        else
            temp = now;

        if(now == nullptr)
            return temp;

        if(temp->data < now->data)
            temp = insert_r(now->left, value);
        else
            temp = insert_r(now->right, value);

        return temp;

        balancing();
    }
    
    auto rotate_left(auto base)
    {
        auto hold = base->right;
        
        base->right = hold->left;
        hold->left = base;
        
        fix_height(base);
        fix_height(hold);

        return hold;
    }

    auto rotate_right(auto base)
    {
        auto hold = base->left;
        
        base->left = hold->right;
        hold->right = base;
        
        fix_height(base);
        fix_height(hold);
        
        return hold;
    }

    void print_pre_order(){
        print_pre_order_recur(root);
        std::cout << std::endl;
    }
    
    void print_pre_order_recur(auto now){
        std::cout << now->data << "Height : " << height(now);
        if (now->left) print_pre_order_recur(now->left);
        if (now->right) print_pre_order_recur(now->right);
    }
};

int main(){
    int n;
    std::cin >> n;

    avl<int> tree;
    tree.input(n);

    tree.print_pre_order();
}
```
## Rekaman Layar

<script src="https://asciinema.org/a/183880.js" id="asciicast-183880" async></script>