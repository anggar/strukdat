# AVL Tree : Basic

## Perbedaan dengan Binary Search Tree

Pada dasarnya AVL Tree merupakan *self-balancing tree*, di mana artinya setiap kali kita input node baru, tree akan otomatis seimbang (*balance*). Seimbang sendiri di sini memiliki arti bahwa ketinggian dari dua buah *subtree*, baik kiri dan kanan sama atau berselisih satu. Sedangkan ketinggian sendiri adalah ukuran kedalaman dari node tersebut ke node *children* paling bawah.