# AVL Tree : Height

## Definisi

Konsep *height* sendiri artinya adalah jarak terjauh dari *root* ke salah satu *child node*. Dalam contoh tree di bawah, tree tersebut memiliki tinggi 3.

![Graph 1](graph1.png)


## Struktur Data

Secara umum, struktur datanya mirip dengan *Binary Search Tree*, namun ada beberapa hal yang membedakan. Salah satunya adalah adanya atribut `height` di struktur data `node`.

```c++
template <class T>
struct node {
    T data;
    int height;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};
```

Dan otomatis hal tersebut membuat fungsi `init_node` berubah, dan menjadi

```c++
auto init_node(T value){
    auto temp = std::make_shared<node<T>>();

    temp->data = value;
    temp->height = 0;
    temp->left  = nullptr;
    temp->right = nullptr;

    return temp;
}
```

Untuk mengetahui *height* dari suatu node, kita bisa membuat suatu fungsi.

```c++
int height(auto base)
{
    return base?base->height: 0;
} 
```

Sedangkan untuk mengeset dan memperbarui nilai height dapat kita buat fungsi `fix_height`.

```c++
void fix_height(auto base)
{
    base->height = (height(base->left) > height(base->right)?(height(base->left) + 1):(height(base->right) + 1));
}
```

## Kode

```c++
#include <iostream>
#include <memory>

template <class T>
struct node {
    T data;
    int height;
    std::shared_ptr<node> left;
    std::shared_ptr<node> right;
};

template <class T>
class avl
{
  private:
    std::shared_ptr<node<T>> root;

  public:
    avl()
    {
        root = nullptr;
    }

    auto init_node(T value){
        auto temp = std::make_shared<node<T>>();

        temp->data = value;
        temp->height = 0;
        temp->left  = nullptr;
        temp->right = nullptr;

        return temp;
    }

    int height(auto base)
    {
        return base?base->height: 0;
    }   

    void fix_height(auto base)
    {
        base->height = (height(base->left) > height(base->right)?(height(base->left) + 1):(height(base->right) + 1));
    }

    void input(int limit){
        T temp;
        while(limit--){
            std::cin >> temp;
            insert(temp);
        }
    }

    void insert(T value){
        root = insert_r(root, value);
    }

    auto insert_r(auto now, T value){
        auto temp = std::make_shared<node<T>>();

        if(now == nullptr){
            temp = init_node(value);
        }
        else{
            temp = now;
        }

        if(now == nullptr)
            return temp;

        if(value < now->data){
            now->left = insert_r(now->left, value);
        }
        else{
            now->right = insert_r(now->right, value);
        }

        fix_height(now);

        return now;
    }
    
    void pre_order_recur(auto now){
        if (now->left) pre_order_recur(now->left);
        if (now->right) pre_order_recur(now->right);
        fix_height(now);
    }
    void print_pre_order(){
        print_pre_order_recur(root);
        std::cout << std::endl;
    }
    
    void print_pre_order_recur(auto now){
        std::cout << now->data << " Height : " << height(now) << std::endl;
        if (now->left) print_pre_order_recur(now->left);
        if (now->right) print_pre_order_recur(now->right);
    }
};

int main(){
    int n;
    std::cin >> n;

    avl<int> tree;
    tree.input(n);

    tree.print_pre_order();
}
```
## Rekaman Layar

<script src="https://asciinema.org/a/184363.js" id="asciicast-184363" async></script>