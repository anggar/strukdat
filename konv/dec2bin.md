# Konversi Bilangan Desimal ke Biner

## Kode

Untuk yang dari desimal sendiri sebenarnya lebih enak menggunakan integer biasa dibanding bitset.

```c++
void decToBin(int number);

int main(){
    int number;
    std::cin >> number;

    decToBin(number);
}

void decToBin(int number){
    std::list<int> binary_representation;

    while(number != 0){
        binary_representation.push_front(number % 2);
        number /= 2;
    }

    for(auto i: binary_representation)
        std::cout << i;

    std::cout << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183362.js" id="asciicast-183362" async></script>