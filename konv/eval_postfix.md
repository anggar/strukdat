# Postfix Evaluator

## Kode

Menyelesaikan persamaan yang ditulis dengan metode RPN (Reverse Polish Notation) atau dikenal juga sebagai Postfix Notation.


```c++
#include <iostream>
#include <list>
#include <string>

int main(){
    std::list<double> numbers;   // Untuk angka yang akan dioperasikan
    std::string element;         // Untuk input

    double temp_result, a, b;

    while(true){
        std::cin >> element;

        try {
            numbers.push_back(stod(element));
        } // Exception handling jika input bukan angka
        catch (const std::exception& e){
            if(element != "="){
                try {
                    // Mengambil 2 input terbelakang
                    a = numbers.back();
                    numbers.pop_back();
                    b = numbers.back();
                    numbers.pop_back();
                }
                catch (const std::exception& r){
                    continue;
                }
            }
            // Masing-masing perintahnya
            if(element == "+"){
                temp_result = b+a;
                numbers.push_back(temp_result);
            }
            else if(element == "-"){
                temp_result = b-a;
                numbers.push_back(temp_result);
            }
            else if(element == "*"){
                temp_result = b*a;
                numbers.push_back(temp_result);
            }
            else if(element == "/"){
                temp_result = b/a;
                numbers.push_back(temp_result);
            }
            else if(element == "="){
                for(auto i: numbers)
                    std::cout << i << " ";

                std::cout << std::endl;
                return 0;
            }
        }
    }
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183365.js" id="asciicast-183365" async></script>