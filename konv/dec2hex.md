# Konversi Bilangan Desimal ke Heksadesimal

## Kode

Untuk yang dari desimal sendiri sebenarnya lebih enak menggunakan integer biasa dibanding bitset.

```c++
#include <iostream>
#include <list>

void decToHex(int number);

int main(){
    int number;
    std::cin >> number;

    decToHex(number);
}

void decToHex(int number){
    std::list<int> hexadecimal_representation;

    while(number != 0){
        hexadecimal_representation.push_front(number % 16);
        number /= 16;
    }

    for(auto i: hexadecimal_representation){
        if(i<10)
            std::cout << i;
        else {
            std::cout << (char)(i-10 + 'A');
        }
    }

    std::cout << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183363.js" id="asciicast-183363" async></script>