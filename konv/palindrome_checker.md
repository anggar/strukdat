# Palindrome Checker

## Kode

Mencari tahu apakah kalimat yang dimasukkan merupakan kalimat palindrom.

```c++
#include <cstdio>
#include <string>
#include <list>

int main(){
    std::list<char> from_front;
    std::list<char> from_back;
    char temp_char = ' ';

    while(true){
        temp_char = getchar();
        if(temp_char == '\n') break;
        // Memanfaatkan kemampuan push_front dan push_back dari
        // std::list
        from_front.push_front(temp_char);
        from_back.push_back(temp_char);
    }

    if(from_front == from_back)
        printf("Palindrom\n");
    else
        printf("Bukan Palindrom\n");
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183368.js" id="asciicast-183368" async></script>