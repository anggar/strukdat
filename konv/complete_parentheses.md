# Complete Parentheses

## Kode

Mencari tahu apakah susunan tanda kurung yang ada seimbang, sehingga tidak ada tanda kurung yang tidak berpasangan.

```c++
#include <iostream>
#include <list>
#include <stack>

bool check_parentheses(std::list<char> input){
    // Stack sebagai tempat menyimpan kurung yang sendirian
    std::stack<char> unpaired_parentheses;
    for(auto q: input){
        if(q == '(')
            unpaired_parentheses.push('(');
        else if(q == ')'){
            if(unpaired_parentheses.empty()) return false;
            unpaired_parentheses.pop();
        }
    }

    // Jika tidak ada yang sendirian, misi berhasil
    return unpaired_parentheses.empty();
}

int main(){
    std::list<char> input;
    char a;

    // Input per karakter
    while(a = getchar()){
        if(a == '\n')
            break;

        input.push_back(a);
    }

    if(check_parentheses(input)){
        std::cout << "Parentheses Complete" << std::endl;
    }
    else {
        std::cout << "Parentheses Incomplete" << std::endl;
    }
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183366.js" id="asciicast-183366" async></script>