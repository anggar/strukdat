# Generate Subset

## Kode

Mencari semua *subset* dari suatu himpunan.

```c++
#include <iostream>
#include <vector>

std::vector<int> subset;
int n = 5;

void search(int k)
{
    if (k == n)
    {
        std::cout << "{ ";
        for(auto q: subset)
            std::cout << q << ' ';

        std::cout << "}";

        std::cout << std::endl;
    }
    else
    {   
        // Pencarian dari yang tidak isinya {}
        search(k + 1);
        subset.push_back(k);
        // Lalu perlahan di isi dari belakang, {4}
        search(k + 1);
        // Kosongkan untuk 1, untuk item yang berbeda
        subset.pop_back();
    }
}

int main(){
    search(0);
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183439.js" id="asciicast-183439" async></script>