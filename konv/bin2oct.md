# Konversi Bilangan Biner ke Octal

## Kode

Yang unik di sini kita kan menggunakan tipe data bitset, di mana tipe data ini sangat mudah untuk melakukan operasi-operasi biner.

```c++
#include <iostream>
#include <list>
#include <bitset>

#define n 20

void binToOct(std::bitset<n> number);

int main(){
    // Deklarasi dan input yang intuitif
    std::bitset<n> number;
    std::cin >> number;

    binToOct(number);
}

void binToOct(std::bitset<n> number){
    // list di sini digunakan sebagai stack
    std::list<std::bitset<n>> octal_representation;

    while(number != 0){
        // Operasi OR untuk mengambil 3 digit terbelakang
        octal_representation.push_front(number & std::bitset<n>(0b111));

        // Shift kiri sebanyak 3 kali
        number = number >> 3;
    }

    // for loop in range merupakan fitur C++ Modern yang dikenalkan
    // pada C++11
    for(auto i: octal_representation){
        // Konversi yang mudah dari bitset ke integer biasa
        // Modulo 8 karena oktal
        std::cout << i.to_ulong()%8;
    }

    std::cout << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183361.js" id="asciicast-183361" async></script>