# Konversi Bilangan Biner ke Hexadesimal

## Kode

Yang unik di sini kita kan menggunakan tipe data bitset, di mana tipe data ini sangat mudah untuk melakukan operasi-operasi biner.

```c++
#include <iostream>
#include <list>
#include <bitset>

#define n 20

void binToHex(std::bitset<n> number);

int main(){
    std::bitset<n> number;
    std::cin >> number;

    binToHex(number);
}

void binToHex(std::bitset<n> number){
    std::list<std::bitset<n>> hexa_representation;

    while(number != 0){
        hexa_representation.push_front(number & std::bitset<n>(0b1111));

        number = number >> 4;
    }

    for(auto i: hexa_representation){
        // Perlakuan yang agak khusus, karena jika sudah lewat 9
        // bilangan diwakili dengan alfabet
        if(i.to_ulong() > 9)      
            std::cout << char(i.to_ulong()%10 + int('A'));
        else
            std::cout << i.to_ulong();
    }

    std::cout << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183356.js" id="asciicast-183356" async></script>