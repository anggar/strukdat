# Generate Permutation

## Kode

Mencari semua kemungkinan permutasi dari suatu set array.

```c++
#include <iostream>
#include <vector>

// Batasan item di array
#define n 5

// Array dinamis
std::vector<int> permutation;
bool chosen[n] = {0};

void permute(){
    if(permutation.size() == n){
        // Range-based loop - fitur dari C++11
        for(auto q: permutation)
            std::cout << q+1 << ' ';

        std::cout << std::endl;
    }
    else {
        for(int i=0; i<n; i++){
            // Jika sudah dipilih, maka tinggalkan, lanjut pilih
            // kemungkinan yang lain
            if(chosen[i]) continue;
            chosen[i] = true;
            permutation.push_back(i);
            permute();
            chosen[i] = false;
            permutation.pop_back();
        }
    }
}

int main(){
    permute();
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183436.js" id="asciicast-183436" async></script>