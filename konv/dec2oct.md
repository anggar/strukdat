# Konversi Bilangan Desimal ke Oktal

## Kode

Untuk yang dari desimal sendiri sebenarnya lebih enak menggunakan integer biasa dibanding bitset.

```c++
#include <iostream>
#include <list>

void decToOct(int number);

int main(){
    int number;
    std::cin >> number;

    decToOct(number);
}

void decToOct(int number){
    std::list<int> octal_representation;

    while(number != 0){
        octal_representation.push_front(number % 8);
        number /= 8;
    }

    for(auto i: octal_representation)
        std::cout << i;

    std::cout << std::endl;
}
```

## Rekaman Layar

<script src="https://asciinema.org/a/183364.js" id="asciicast-183364" async></script>