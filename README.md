# Final Project Struktur Data F

    Nama : Anggar Wahyu Nur Wibowo
    NRP  : 05111740000052

FP ini bisa di akses di [anggar.gitlab.io/strukdat](http://anggar.gitlab.io/strukdat).

## Tampilan

Di sini saya menggunakan platform gitbook untuk mengumpulkan semua dokumen yang ada. Untuk kodenya langsung saya masukkan di dalamnya, dan penjelasan berupa komentar di dalam kode. Selain itu untuk tangkapan layar, saya menggunakan *platform* [Asciinema](http://asciinema.org) sehingga bisa menampilkan riwayat eksekusi di konsol terminal.

